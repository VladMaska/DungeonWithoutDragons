/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;

public class ExitScript : MonoBehaviour {

    private void OnTriggerEnter2D( Collider2D collision ){

        //if ( collision.gameObject.tag == "Player" && LevelCore._demons == 0 ){

        //    GameCore.level++;
        //    GameObject.Find( "Core" ).GetComponent<LevelCore>().ShowLevel();
        //    GameObject.Find( "Render" ).GetComponent<LevelRender>().renderLevel();

        //}

        if ( collision.gameObject.tag == "Player" ){

            GameCore.level++;
            GameObject.Find( "Core" ).GetComponent<LevelCore>().ShowLevel();
            GameObject.Find( "Render" ).GetComponent<LevelRender>().renderLevel();

        }
        
    }

}