/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 
                                                                                                   
*/

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;

[System.Serializable]
public class RenderObjects {

    public Color color;

    [ShowAssetPreview]
    public GameObject obj; 

}

public class LevelRender : MonoBehaviour {

    [HorizontalLine]

    [ShowAssetPreview]
    public GameObject hero;

    [HorizontalLine]

    [ShowAssetPreview]
    public Texture2D map;

    [HorizontalLine]
    [Range( 0, 20 )]
    public int n = 10;
    [HorizontalLine]

    public RenderObjects[] obj; 

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    static int mapW, mapH;
    int oldX, oldY;
    Vector2 tp = Vector2.zero;

    public Texture2D[] mazes;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Start(){

        tp = hero.transform.position;

        mazes = Resources.LoadAll<Texture2D>( "Mazes" );

        map = mazes[ GameCore.level - 1 ];
        Render();

        oldX = (int)tp.x;
        oldY = (int)tp.y;

        mapW = map.width;
        mapH = map.height;

        hero.transform.position = new Vector3( ( mapW / 2 ) - 2, mapH, -1 );
        
    }

    void Update(){

        tp = hero.transform.position;

        if ( Mathf.RoundToInt( tp.x ) != oldX || Mathf.RoundToInt( tp.y ) != oldY ){ Render(); }

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Render(){

        ClearChildren();

        for ( int i = (int)tp.x - n; i <= tp.x + n; i++ ){

            for ( int j = (int)tp.y - n; j <= tp.y + n; j++ ){

                Color pixelColor = map.GetPixel( i , j );

                foreach ( RenderObjects ro in obj ){

                    if ( ro.color.Equals( pixelColor ) ){

                        Vector2 pos = new Vector2( i, j );

                        GameObject _obj = Instantiate( ro.obj, pos, Quaternion.identity, this.transform );
                        _obj.name = ro.obj.name;

                    }

                }

            }

        }

        oldX = Mathf.RoundToInt( tp.x );
        oldY = Mathf.RoundToInt( tp.y );

    }

    void ClearChildren(){

        int i = 0;
        GameObject[] allChildren = new GameObject[ transform.childCount ];
        foreach ( Transform child in transform ){ allChildren[ i ] = child.gameObject; i += 1; }
        foreach ( GameObject child in allChildren ){ DestroyImmediate( child.gameObject ); }

    }

    public void renderLevel(){

        map = mazes[ GameCore.level - 1 ];
        SceneManager.LoadScene( 1 );

    }

    public void playerToStart(){

        mapW = map.width;
        mapH = map.height;

        hero.transform.position = new Vector3( ( mapW / 2 ) - 2, mapH, -1 );

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static int MaxX(){ return mapW - 1; }

    public static int MaxY(){ return mapH - 1; }

}