/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;
using WhiteWolf;

public class ChestScript : MonoBehaviour {

    [HorizontalLine]

    [ShowAssetPreview]
    public Sprite itemInChestSprite;
    public string itemInChest;
    public int itemsN;

    [HorizontalLine]

    [ProgressBar( "HP", 5, EColor.Red )]
    public int hp = 5;

    public TextMesh text;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    Animator anim;
    Vector2 tp;
    bool near;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Start() => anim = this.gameObject.GetComponent<Animator>();

    void Update(){

        tp = transform.position;

        text.GetComponent<TextMesh>().text = $"{hp} / 5";

        if ( hp == 0 ){ StartCoroutine( Opened() ); }
        
    }

    private void OnMouseDown(){

        if ( Input.GetMouseButtonDown( 0 ) && hp != 0 ) hp--;
        else if ( Input.GetMouseButtonDown( 1 ) && hp != 0 ) hp = 0;
        
    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    private void OnTriggerEnter2D( Collider2D collision ){

        if ( collision.gameObject.tag == "Wall" ){

            transform.position = WeHave.BUG();

        }
        
    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    IEnumerator Opened(){

        anim.Play( "Open" );

        yield return new WaitForSeconds( 2.5f );

        WW_Database.PlusInt( itemInChest, itemsN );

        Destroy( this.gameObject );

    }

}