/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;

namespace WhiteWolf {

    [System.Serializable]
    public class Traps {

        public string name;

        [Space]

        public Color color;

        [ShowAssetPreview]
        public GameObject element;

    }

    public class TrapGenerator : MonoBehaviour {

        public static Texture2D map;
        public Transform level;
        public Traps[] elements;

        /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

        int pos_x, pos_y;
        Texture2D[] traps;

        /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

        void Start() => Generator();

        /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

        public void Generator(){

            traps = Resources.LoadAll<Texture2D>("Traps");
            map = traps[ GameCore.level - 1 ];

            //pos_x = -( map.width / 2 );
            //pos_y = -( map.height / 2 );

            pos_x = 0;
            pos_y = 0;

            for ( int x=0; x<map.width; x++ ){

                for ( int y=0; y<map.height; y++ ){

                    GenerateTile( x, y );

                }

            }

        }

        void GenerateTile( int x, int y ){

            Color pixelColor = map.GetPixel( x, y );

            if ( pixelColor.a == 0 ){ return;  }

            foreach ( Traps el in elements ) {

                if ( el.color.Equals( pixelColor ) ){

                    Vector3 pos = new Vector3( pos_x + x, pos_y + y, -0.5f );
                    GameObject obj = Instantiate ( el.element, pos, Quaternion.identity, level );
                    obj.name = el.name;

                }

            }

        }

    }

}