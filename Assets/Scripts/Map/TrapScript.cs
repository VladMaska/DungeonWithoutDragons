/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 
                                                                                                   
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;

public class TrapScript : MonoBehaviour {

    [HorizontalLine]

    public float time;
    public float timer;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    Animator anim;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Start(){

        time = Random.Range( 3, 6.66f );

        anim = this.gameObject.GetComponent<Animator>();
        
    }

    void Update(){

        if ( timer >= time ){ StartCoroutine( AnimWork() ); }
        else { timer += Time.deltaTime; }
        
    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    IEnumerator AnimWork(){

        anim.SetBool( "work", true );

        yield return new WaitForSeconds( 1 );

        anim.SetBool( "work", false );
        timer = 0;

    }

}