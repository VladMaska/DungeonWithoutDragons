/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 

*/

//Relinquere spem, quisque qui intrat hic

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;
using WhiteWolf;

public class TheEnd : MonoBehaviour {

    public GameObject end;
    public TextMesh text;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    GameCore gc;
    int yes = 0;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Start(){

        gc = GameObject.Find( "Core" ).GetComponent<GameCore>();
        gc.panel.SetActive( false );

        if ( WW_Database.LoadDataInt( "killed_demons" ) >= 12 ){
            gc.panelText.GetComponent<Text>().text = "Wow, do you think\nI'll let you go after this?";
        }
        else {
            gc.panelText.GetComponent<Text>().text = "Thank you for helping my demons";
        }
        
    }

    private void OnMouseDown(){

        gc.panel.SetActive( true );
        
    }

    /*『 UI 』*/
    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public void Yes(){

        switch ( yes ){

            case 0:
                gc.panelText.GetComponent<Text>().text = "『 Leave hope,\neveryone who enters here 』\n That's what it says there";
                break;

            case 1:
                SceneManager.LoadScene( 0 );
                break;

        }

        yes++;

    }

}