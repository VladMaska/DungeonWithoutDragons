/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 
                                                                                                   
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;

[System.Serializable]
public class GiveSettings {

    [ShowAssetPreview]
    public Sprite need;

    public int needN;
    public string item;

}

public class EnemyScript : MonoBehaviour {

    [Label("Give Settings")]
    public GiveSettings gs;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    bool selected;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Start(){
        
    }

    void Update(){

        if ( selected && Input.GetKeyDown( KeyCode.K ) ){

            LevelCore.PlusKilledDemon();

        }

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    private void OnMouseDown(){

        if ( Input.GetMouseButtonDown( 0 ) ){

            ChoiseScript.Show( gs.need, gs.item, gs.needN, this.gameObject );
            GameCore.choise = true;

        }
        
    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    private void OnTriggerEnter2D( Collider2D collision ){

        if ( collision.gameObject.tag == "Wall" ){
            transform.position = WeHave.BUG();
        }
        
    }

}