/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 
                                                                                                   
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;
using WhiteWolf;

public class ChoiseCore : MonoBehaviour {

    [Button]
    void TestButton() => print( Check( "test", 1 ) );

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Start(){


        
    }

    void Update(){


        
    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static bool Check( string item, int amount ){

        if ( WW_Database.LoadDataInt( item ) >= amount ){ return true; }
        return false;

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static void MinusItem( string item, int amount ){

        WW_Database.SaveDataInt(
            item,
            WW_Database.LoadDataInt( item, -1 - amount ) - amount
            );

    }

    public static void PlusItem( string item, int amount ){

        WW_Database.SaveDataInt(
            item,
            WW_Database.LoadDataInt( item, -1 - amount ) + amount
            );

    }

}