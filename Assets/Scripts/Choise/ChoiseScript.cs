/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 
                                                                                                   
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;
using WhiteWolf;

public class ChoiseScript : MonoBehaviour {

    public Text needN;

    public Image whatGive;
    public Text quantity;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static int needn;

    public static Sprite whatGiveSprite;
    public static int quantityInt;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    static string itemName;
    static GameObject _enemy;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Update(){

        needN.GetComponent<Text>().text = $"Give me {needn}";

        whatGive.GetComponent<Image>().sprite = whatGiveSprite;
        quantity.GetComponent<Text>().text = WW_Database.LoadDataInt( itemName, -1 ).ToString();
        
    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static void Show( Sprite sprite, string item, int need, GameObject enemy ){

        GameCore.choise = true;

        needn = need;
        whatGiveSprite = sprite;

        itemName = item;

        _enemy = enemy;

    }

    /* UI */
    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public void No(){

        if ( WW_Database.LoadDataInt( itemName ) < needn ){

            GameCore.choise = false;

            GameCore.message = true;
            MessageScript.Show( "I all\nunderstand" );

            WW_Database.SaveDataInt( itemName, WW_Database.LoadDataInt( itemName ) + Random.Range( 0, needn ) );

        }

    }

    public void GiveB(){

        if ( WW_Database.LoadDataInt( itemName ) >= needn ){

            WW_Database.SaveDataInt( itemName, WW_Database.LoadDataInt( itemName ) - needn );
            GameCore.choise = false;

            GameCore.message = true;
            MessageScript.Show( "Thank you", 250 );

            LevelCore._demons--;
            Destroy( _enemy );

        }
        else {

            print( "<color=red>Error</color>" );
            ErrorScript.Error( whatGiveSprite, needn );

        }

    }

    public void Exit() => GameCore.choise = false;

}