/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/

*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using WhiteWolf;

public class GameCore : MonoBehaviour {

    /*–––––––| Main |–––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static int level = 1;
    public int l = level;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/



    /*–––––––| DataBase items |–––––––––––––––––––––––––––––––––––––––––––––––*/

    public static string item_red_bottle = "red_bottle";
    public static string item_blue_bottle = "blue_bottle";
    public static string item_key = "key";

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    

    /*–––––––| UI |–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static bool menu = false;
    public static bool choise = false;
    public static bool error = false;
    public static bool message = false;
    public static bool inventory = false;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/



    /*–––––––| Player |–––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static string heroName = "Player";
    public static float _speed = 1.5f;
    public static int heroHp = 8;

    public static string heroHPdb = "playerHP";

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/



    /*–––––––| Player Tourch |––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static bool tourch;
    public static float standartLightLevel = 2f;
    public static float lightWithTorch = 4f;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/



    /*–––––––| Other |––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static int enemyN;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/



    /*–––––––| 『 Diabolus 』 |––––––––––––––––––––––––––––––––––––––––––––––––*/

    public GameObject panel;
    public Text panelText;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/



    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static void NextLevel() => level++;

    public static void PreviousLevel() => level--;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    private void Update() => WW_Database.SaveDataInt( heroHPdb, heroHp );

}