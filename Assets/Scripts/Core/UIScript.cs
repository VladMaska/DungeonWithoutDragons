/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 
                                                                                                   
*/

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

[System.Serializable]
public class MenuUI {

    public KeyCode key;
    public GameObject panel;

}

[System.Serializable]
public class ChoiseUI {

    public GameObject panel;

}

[System.Serializable]
public class ErrorUI {

    public GameObject panel;

}

[System.Serializable]
public class MessageUI {

    public GameObject panel;

}

[System.Serializable]
public class InventoryUI {

    public KeyCode key;
    public GameObject panel;

}

/*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

public class UIScript : MonoBehaviour {

    public MenuUI menu;
    [HorizontalLine]

    public ChoiseUI choise;
    [HorizontalLine]

    public ErrorUI error;
    [HorizontalLine]

    public MessageUI message;
    [HorizontalLine]

    [Label("Inventory")]
    public InventoryUI inv;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Update(){

        Menu();

        Choise();
        Error();
        Message();

        Inventory();

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Menu(){

        menu.panel.SetActive( GameCore.menu );
        if ( Input.GetKeyDown( menu.key ) ){

            GameCore.menu = !GameCore.menu;
            AudioCore.SwitchAudio();

        }

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Choise() => choise.panel.SetActive( GameCore.choise );

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Error() => error.panel.SetActive( GameCore.error );

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Message() => message.panel.SetActive( GameCore.message );

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Inventory(){
        inv.panel.SetActive( GameCore.inventory );
        if ( Input.GetKeyDown( inv.key ) ){ GameCore.inventory = !GameCore.inventory; }
    }

}