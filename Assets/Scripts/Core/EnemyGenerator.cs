/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;

/*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

[System.Serializable]
public class EnemyGeneratorSettings {

    [Label( "level" )]
    public string name;

    [HorizontalLine]

    public int min;
    public int max;

}

[System.Serializable]
public class Enemies {

    public string name;

    [ShowAssetPreview]
    public GameObject enemy;

}

[System.Serializable]
public class Items {

    public string name;

    [HorizontalLine]

    [ShowAssetPreview]
    public GameObject item;

}

/*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

public class EnemyGenerator : MonoBehaviour {

    public Transform enemiesSpawn;
    public Transform chestsSpawn;

    [HorizontalLine]

    [Label("Enemy Generator Settings")]
    public EnemyGeneratorSettings[] eGs;

    [HorizontalLine]
    
    public Enemies[] enemies;

    [HorizontalLine]

    public Items[] items;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    [HorizontalLine]

    [Label("Chest")]
    [ShowAssetPreview]
    public GameObject chestObj;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    [HorizontalLine]

    public int redbottles;
    public int bluebottles;
    public int keys;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    [Button("Names")]
    void Names(){ for ( int i=0; i<enemies.Length; i++ ){ enemies[ i ].name = enemies[ i ].enemy.name; } }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Start(){

        if ( GameCore.level != 9 ){ StartCoroutine( waitPlease() ); } }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void GenerateAll(){

        int maxX = LevelRender.MaxX();
        int maxY = LevelRender.MaxY();

        int enemyN = Random.Range( eGs[ GameCore.level - 1 ].min, eGs[ GameCore.level - 1 ].max );
        LevelCore._demons = enemyN;

        for ( int i=0; i<enemyN; i++ ){

            Vector3 enemyPos = new Vector3( Random.Range( 1, maxX ), Random.Range( 1, maxY ), -1 );
            GameObject e = enemies[ Random.Range( 0, enemies.Length ) ].enemy;

            GameObject obj = Instantiate( e, enemyPos, Quaternion.identity, enemiesSpawn );
            obj.name = $"{e.name}_{i}";

            int needItem = Random.Range( 0, items.Length );

            EnemyScript enemiescript = obj.GetComponent<EnemyScript>();

            enemiescript.gs.need = items[ needItem ].item.GetComponent<SpriteRenderer>().sprite;
            enemiescript.gs.needN = Random.Range( 1, 5 );
            enemiescript.gs.item = items[ needItem ].name;

            switch ( needItem ){

                case 0:
                    redbottles++;
                    break;

                case 1:
                    bluebottles++;
                    break;

            }

        }

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void GenerateChests(){

        int maxX = LevelRender.MaxX();
        int maxY = LevelRender.MaxY();


        /* RED bottle */
        for ( int i=0; i<redbottles; i++ ){

            Vector3 chestPos = new Vector3( Random.Range( 1, maxX ), Random.Range( 1, maxY ), -1 );
            GameObject chest = Instantiate( chestObj, chestPos, Quaternion.identity, chestsSpawn );
            chest.name = $"RChest_{i}";

            ChestScript cs = chest.GetComponent<ChestScript>();

            cs.itemInChestSprite = items[ 0 ].item.GetComponent<SpriteRenderer>().sprite;
            cs.itemsN = Random.Range( 1, 3 );
            cs.itemInChest = items[ 0 ].name;

        }

        /* BLUE bottle */
        for ( int i=0; i<bluebottles; i++ ){

            Vector3 chestPos = new Vector3( Random.Range( 1, maxX ), Random.Range( 1, maxY ), -1 );
            GameObject chest = Instantiate( chestObj, chestPos, Quaternion.identity, chestsSpawn );

            chest.name = "BChest";

            ChestScript cs = chest.GetComponent<ChestScript>();

            cs.itemInChestSprite = items[ 1 ].item.GetComponent<SpriteRenderer>().sprite;
            cs.itemsN = Random.Range( 1, 3 );
            cs.itemInChest = items[ 1 ].name;

        }

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    IEnumerator waitPlease(){

        yield return new WaitForSeconds( 0.5f );
        GenerateAll();
        GenerateChests();

    }

}