/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioCore : MonoBehaviour {

    public AudioSource menuAudio;
    public AudioSource hellAudio;

    public bool m;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static bool menu;

    static AudioSource _menuAudio;
    static AudioSource _hellAudio;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Start(){

        menu = GameCore.menu;

        _hellAudio = hellAudio;
        _menuAudio = menuAudio;

        SwitchAudio();
        
    }

    void Update() => m = menu;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static void SwitchAudio(){

        menu = GameCore.menu;

        if ( menu ){

            _menuAudio.Play();
            _hellAudio.Stop();

        }
        else {

            _menuAudio.Stop();
            _hellAudio.Play();

        }

    }

}