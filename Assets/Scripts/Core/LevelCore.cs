/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 
                                                                                                   
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;
using WhiteWolf;

public class LevelCore : MonoBehaviour {

    [HorizontalLine]

    public int demons;
    public int killed;

    [HorizontalLine]

    public GameObject Panel;
    public Text text;

    [Button]
    void Show() => StartCoroutine( showlevel() );

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static int _demons;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Update(){

        demons = _demons;
        killed = WW_Database.LoadDataInt( "killed_demons" ); 

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public void ShowLevel() => StartCoroutine( showlevel() );

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public static void PlusKilledDemon() => WW_Database.PlusInt( "killed_demons", 1 );

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    IEnumerator showlevel(){

        text.GetComponent<Text>().text = $"Welcome to the\n{GameCore.level}th circle";
        Panel.SetActive( true );

        yield return new WaitForSeconds( 6.66f );

        Panel.SetActive( false );

    }

}