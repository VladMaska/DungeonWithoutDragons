/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 
                                                                                                   
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using WhiteWolf;

public class UIInv : MonoBehaviour {

    public void Health(){

        if ( GameCore.heroHp < 9 && WW_Database.LoadDataInt( GameCore.item_red_bottle ) > 0 ){

            GameCore.heroHp++;
            WW_Database.SaveDataInt( GameCore.item_red_bottle, WW_Database.LoadDataInt( GameCore.item_red_bottle ) - 1 );

        }

    }

    public void FullHP(){

        if ( GameCore.heroHp < 9 && WW_Database.LoadDataInt( GameCore.item_blue_bottle ) > 0 ){

            GameCore.heroHp = 9;
            WW_Database.SaveDataInt( GameCore.item_blue_bottle, WW_Database.LoadDataInt( GameCore.item_blue_bottle ) - 1 );

        }

    }

}