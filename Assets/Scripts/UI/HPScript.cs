/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 
                                                                                                   
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;

public class HPScript : MonoBehaviour {

    [Button]
    void P() => GameCore.heroHp++;

    [Button]
    void M() => GameCore.heroHp--;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    [Range( 1, 9 )]
    public int hpN;

    [HorizontalLine]

    [ShowAssetPreview]
    public Sprite plus;

    [ShowAssetPreview]
    public Sprite minus;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    Image image;
    Sprite sprite;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Start(){ image = this.gameObject.GetComponent<Image>(); sprite = plus; }

    void Update(){

        image.sprite = sprite;

        if ( GameCore.heroHp >= hpN ){ Plus(); }
        else { Minus(); }

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    public void Plus() => sprite = plus;

    public void Minus() => sprite = minus;

}