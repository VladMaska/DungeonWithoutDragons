/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 
                                                                                                   
*/

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Experimental.Rendering.Universal;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;
using WhiteWolf;

public class Player : MonoBehaviour {

    //public Light light;
    public Light2D _light;

    [ShowAssetPreview]
    public GameObject tourchObject;

    [HorizontalLine]

    public GameObject render;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    LevelRender lr;
    Rigidbody2D rb;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Start(){

        this.gameObject.name = GameCore.heroName;
        GameCore.heroHp = WW_Database.LoadDataInt( GameCore.heroHPdb, 9 );

        lr = render.GetComponent<LevelRender>();

        rb = this.gameObject.GetComponent<Rigidbody2D>();

    }

    void Update(){

        Move();
        Rotate();
        Tourch();

        if ( GameCore.heroHp == 0 ){ YouDead(); }
        
    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Move(){

        float x = Input.GetAxis( "Horizontal" ) * GameCore._speed;
        float y = Input.GetAxis( "Vertical" ) * GameCore._speed;

        rb.velocity = new Vector2 ( x, y );

    }

    void Rotate(){

        float rotate = Input.GetAxis( "Horizontal" );

        if ( rotate > 0 ){

            this.transform.eulerAngles = new Vector2( 0, 0 );

        }
        else if ( rotate < 0 ){

            this.transform.eulerAngles = new Vector2( 0, 180 );

        }

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Tourch(){

        if ( Input.GetKeyDown( KeyCode.E ) && !GameCore.tourch ){ GameCore.tourch = true; }
        else if ( Input.GetKeyDown( KeyCode.E ) && GameCore.tourch ){ GameCore.tourch = false; }

        switch ( GameCore.tourch ){

            case true:
                _light.pointLightOuterRadius = GameCore.lightWithTorch;
                tourchObject.SetActive( true );
                break;

            case false:
                _light.pointLightOuterRadius = GameCore.standartLightLevel;
                tourchObject.SetActive( false );
                break;

        }

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void YouDead(){

        WW_Database.ResetData();

        WW_Database.SaveDataInt( GameCore.heroHPdb, 9 );
        GameCore.level = 1;
        SceneManager.LoadScene( 1 );

    }

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    private void OnTriggerEnter2D( Collider2D collision ){

        if ( collision.gameObject.tag == "Trap" ){

            GameCore.heroHp--;

        }
        
    }

}