/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 

*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

public class ExitArrowScript : MonoBehaviour {

    [ShowAssetPreview]
    public GameObject arrow;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    private void OnTriggerEnter2D( Collider2D collision ){

        if ( collision.gameObject.tag == "Player" ){ arrow.SetActive( true ); }
        
    }

    private void OnTriggerExit2D( Collider2D collision ){

        if ( collision.gameObject.tag == "Player" ){ arrow.SetActive( false ); }
        
    }

}