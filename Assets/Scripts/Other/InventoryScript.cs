/*
 __   __   __         ______     _____     __    __     ______     ______     __  __     ______    
/\ \ /  / /\ \       /\  __ \   /\  __-.  /\ "-./  \   /\  __ \   /\  ___\   /\ \/ /    /\  __ \   
\ \ \' /  \ \ \____  \ \  __ \  \ \ \/\ \ \ \ \-./\ \  \ \  __ \  \ \___  \  \ \  _"-.  \ \  __ \  
 \ \__/    \ \_____\  \ \_\ \_\  \ \____-  \ \_\ \ \_\  \ \_\ \_\  \/\_____\  \ \_\ \_\  \ \_\ \_\ 
  \/_/      \/_____/   \/_/\/_/   \/____/   \/_/  \/_/   \/_/\/_/   \/_____/   \/_/\/_/   \/_/\/_/ 
                                                                                                   
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using NaughtyAttributes;
using WhiteWolf;

[System.Serializable]
public class HPBottle {

    public Image image;
    public Text text;

}

[System.Serializable]
public class BlueBottle {

    public Image image;
    public Text text;

}

[System.Serializable]
public class Keys {

    public Image image;
    public Text text;

}

/*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

public class InventoryScript : MonoBehaviour {

    public HPBottle hp;
    public BlueBottle bb;
    public Keys keys;

    /*––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––*/

    void Update(){

        hp.text.GetComponent<Text>().text = $"  Amount: { WW_Database.LoadDataInt( GameCore.item_red_bottle, 0 ) }";

        bb.text.GetComponent<Text>().text = $"  Amount: { WW_Database.LoadDataInt( GameCore.item_blue_bottle, 0 ) }";

        keys.text.GetComponent<Text>().text = $"  Amount: { WW_Database.LoadDataInt( GameCore.item_key, 0 ) }";
        
    }

}